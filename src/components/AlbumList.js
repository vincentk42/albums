import React, { Component } from 'react';
import { ScrollView, } from 'react-native';
import AlbumDetail from './AlbumDetail';

class AlbumList extends Component {
    // this.state = {
    //     albums: []
    // };


    constructor() {
        super();
        this.state = {
            albums: [],
        };
    }

    componentWillMount() {
        console.log('component did mount');
        fetch('https://rallycoding.herokuapp.com/api/music_albums')
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    albums: responseData
                });
            });
    }

    renderAlbums() {
        return this.state.albums.map(album => 
            /*best key choice is if the incoming object has a unique 
            id*/
            <AlbumDetail 
                album={album}
                key={album.title} 
            />
        );
    }

    render() {
        console.log('hello');
        console.log(this.state);
        return (
            <ScrollView>
                {this.renderAlbums()}
            </ScrollView>
        );
    }
}

export default AlbumList;